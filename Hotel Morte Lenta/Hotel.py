#No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei
#A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
#Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

import sys
import random
#Pedir o nome ao usuário:
def GetName():
	return input ("Nome: ")

#Validar se o nome inserido é string:
def ValidateString(name):
	if all (x.isalpha() or x.isspace() for x in name):
		return True

		

#Validar se o nome possui 3 vogais ou mais, nenhuma sendo O ou U:
def ValidateName(name):
    vgAEI=0
    for x in name:
    	if x in "aei":
    		vgAEI += 1
    return vgAEI >= 3 and "o" not in name.lower() and "u" not in name.lower()

# Analisa o gênero com base em um "banco de dados":
def GetSex(name):
	M = ["Renato Faca", "Roberto Arma", "Uesllei"]
	F = ["Maria Cálice", "Roberta Corda"]
	if name.title() in M:
		return "M"
	elif name.title() in F:
		return "F"
	else:
		sys.exit("Não foi possivel definir o gênero do suspeito")
#Definir a arma com base no nome e no "banco de dados":
def GetWeapon(name):
	ArmasBrancas = ["faca", "corda", "cálice"]
	for x in ArmasBrancas:
		if x in name.lower():
			B = True
		else: 
			B = False
	return B

#Validar se é uma mulher assasina, retorna True se o sexo for feminino e tiver utilizado uma arma branca:
def ValidateWoman(GetSex, B):
	if GetSex == "F" and B:
		return True
	
#Validar se é um homem assasino, retorna True se o sexo for masculino e estiver no saguão as 00:30:
def ValidateMan(GetSex, time):
	if GetSex == "M" and time == "(0, 30)":
		return True
# Sorteia um numero entre 0 e 23 para ser a hora, e outro entre 0 e 59 para os minutos
def GetTime():
	hour = random.randint(0, 23)
	minute = random.randint(0, 59)
	time = (hour,minute)
	return time 

print("-"*65)
print("Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.")
print("-"*65)

name = GetName()

VS = ValidateString(name)
VN = ValidateName(name)
AB = GetWeapon(name)
Gender = GetSex(name)

if not VS:
	sys.exit("Você inseriu um caractere inválido.")

if not VN:
	sys.exit("Não é o assasino.")

vw = ValidateWoman(Gender, AB)
vm = ValidateMan(GetSex, GetTime)

if not vw or vm:
	sys.exit("Não é o(a) assasino(a).")
print ("Assasino(a)!!!")