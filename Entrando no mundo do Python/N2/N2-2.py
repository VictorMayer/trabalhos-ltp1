#cria e atribui o valor 300 a variavel cebolas.
cebolas = 300
#cria e atribui o valor 120 a variavel cebolas_na_caixa.
cebolas_na_caixa = 120
#cria e atribui o valor 5 a variavel espaco_caixa.
espaco_caixa = 5
#cria e atribui o valor 60 a variavel caixas.
caixas = 60
#cria e atribui um valor variavel cebolas_fora_da_caixa.
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#cria e atribui um valor a variavel caixas_vazias.
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#cria e atribui um valor a variavel caixas_necessarias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#executa a funcao print para exibir uma mensagem (faltavam os parenteses para indicar que era uma funcao)
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#executa a funcao print para exibir uma mensagem (faltavam os parenteses para indicar que era uma funcao)
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#executa a funcao print para exibir uma mensagem (faltavam os parenteses para indicar que era uma funcao)
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
#executa a funcao print para exibir uma mensagem (faltavam os parenteses para indicar que era uma funcao)
print ("Ainda temos,", caixas_vazias, "caixas vazias")
#executa a funcao print para exibir uma mensagem (faltavam os parenteses para indicar que era uma funcao)
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")