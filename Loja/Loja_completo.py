import sys
total=0
totdiscount=0
final=0

def Ask(msg):
	try:
		return float(input(msg))
	except:
		sys.exit("Valor invalido")

def Resp():
	resp=input("Deseja continuar? [S/N]: ")
	if resp.upper() in 'N':
		print("-"*60)
		print("O valor da compra seria R$",total)
		print("Com a aplicação do desconto, o valor final ficou R$",final)
		print("-"*60)
		sys.exit("Compra finalizada!")
	elif resp.upper() not in 'NS':
		sys.exit("Caractere invalido!")

def Calculate(x, y):
	calc=x - (x * y / 100)
	return calc

while True:
	price=Ask("Insira o preco do produto: ")
	discount=Ask("Insira o valor do desconto [0 - 100]: ")
	total+=price
	final+=Calculate(price, discount)
	Resp()