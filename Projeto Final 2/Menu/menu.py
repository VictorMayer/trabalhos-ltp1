from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.mineiro import Mineiro
import sys
import os
class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""\033[1;32m
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar\033[m
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""\033[1;32m
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedades\033[m
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()

          # Consultar
            if opMenu == "1":
                entrada("Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        if (retorno != None):
                            print(retorno)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print('Não encontrado')
                    elif opMenu == "0":
                        print("\033[1;32mVoltando\033[m")
                opMenu = ""


            # Inserir
            elif opMenu == "2":
                entrada("Inserir")
                Menu.menuInserir(d)


            # Alterar
            elif opMenu == "3":
                entrada("Alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        entrada("1;31mconsultar por Identificador\033[m")
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        Menu.menuAlterar(retorno,d)
                    elif opMenu == "2":
                        print('entrou no consultar por Propriedade')
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("[1;31mNao encontrado\033[m")

                    elif opMenu == "0":
                        print("[1;31mVoltando\033[m")
                opMenu = ""


            # Deletar
            elif opMenu == "4":
                entrada("Deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        entrada("consultar por Identificador")
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        if retorno != None:
                          Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        print("entrou no consultar por Propriedade")
                        retorno = Menu.menuBuscarPorAtributo(d)
                        Menu.menuDeletar(d,retorno)
                    elif opMenu == "0":
                        print("[1;31mVoltando\033[m")
                opMenu = ""
            elif opMenu == "0":
                print("\033[1;31mSaindo\033[m")
            elif opMenu == "":
                print("")
            else:
                print("\033[1;31mInforme uma opcao valida\033[m")

    @staticmethod
    def menuInserir(d):
        m = Mineiro()
        m.nome         = input('\033[1;32mInforme o nome: \033[m')
        for x in m.nome:
            if not(x.isalpha() or x.isspace()):
                print('\033[1;31mCaractere invalido! Insira apenas letras!\033[m')
                return Menu.menuInserir(d)

        m.cpf          = input('\033[1;32mInforme o CPF: \033[m')
        if not m.cpf.isnumeric() or len(m.cpf) != 11:
            print('\033[1;31mCaractere ou quantidade de digitos invalido!\033[m')
            return Menu.menuInserir(d)
        m.peso         = input('\033[1;32mInforme o peso em kg: \033[m')
        if not m.peso.isnumeric():
            print('\033[1;31mCaractere invalido! Insira apenas numeros!\033[m')
            return Menu.menuInserir(d)

        m.tamanho      = input('\033[1;32mInforme a altura em centimetros: \033[m')
        if not m.tamanho.isnumeric():
            print('\033[1;31mCaractere invalido! Insira apenas numeros!\033[m')
            return Menu.menuInserir(d)

        m.tipoDeCabelo = input('\033[1;32mInforme o tipo de cabelo: \033[m')
        if not m.tipoDeCabelo.isalpha():
            print('\033[1;31mCaractere invalido! Insira apenas letras!\033[m')
            return Menu.menuInserir(d)
        d.inserir(m)

    def menuAlterar(retorno,d):
      retorno.nome = Validador.validarValorInformado(retorno.nome,"Informe o nome: ")
      retorno.cpf = Validador.validarValorInformado(retorno.cpf,"Informe o CPF: ") 
      retorno.peso = Validador.validarValorInformado(retorno.peso,"Informe um peso: ") 
      retorno.tamanho = Validador.validarValorInformado(retorno.tamanho,"Informe o tamanho: ") 
      retorno.tipoDeCabelo = Validador.validarValorInformado(retorno.tipoDeCabelo,"Informe o tipo de cabelo: ") 
      d.alterar(retorno)

    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input("""
            Deseja deletar o registro:
            [S] - Sim
            [N] - Não
            """)
        if (resposta == "S" or resposta == "s"):
            d.deletar(entidade)
            print("Registro deletado")
        else:
            print("""Registro nao foi deletado""")

    @staticmethod
    def menuBuscarPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscarPorAtributo(d : Dados):
        retorno = d.buscarPorAtributo(input("\033[1;32mInforme um tamanho:\033[m"))
        return retorno

def entrada(msg):
    print('\033[1;32m-\033[m'*30)
    print(f'\033[1;32mVoce entrou em, {msg}\033[m')
    print('\033[1;32m-\033[m'*30)